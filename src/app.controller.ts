import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

// /products
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // /products/user
  @Get()
  getHello() {
    return this.appService.getUser();
  }
}
