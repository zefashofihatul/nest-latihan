import { Prisma } from '@prisma/client';
import { IsNotEmpty, IsEmail, IsString, NotContains } from 'class-validator';

export class SignupDto implements Prisma.UserCreateInput {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @NotContains("'")
  @NotContains('"')
  name: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
