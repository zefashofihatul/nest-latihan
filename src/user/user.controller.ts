import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from './user.service';
// import { CreateUserDto } from './dto/create-user.dto';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('get-user-1')
  async findAll() {
    return this.userService.findAll();
  }

  @Get('get-user-2')
  async findAll2() {
    return this.userService.findAll2();
  }

  @Get('get-user-3')
  async findAll3() {
    return this.userService.findAll3();
  }

  @Get('axios-user')
  async axiosGet() {
    return await this.userService.axiosGet1();
  }

  @Post('axios-post')
  async axiosPost(@Body() body) {
    return await this.userService.axiosPost1(body);
  }
}
