import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Prisma } from '@prisma/client';
import axios from 'axios';

@Injectable()
export class UserService {
  constructor(private dbService: PrismaService) {}

  async createUser(data: Prisma.UserCreateInput) {
    return this.dbService.user.create({
      data,
    });
  }

  async findAll() {
    const result = await this.dbService.user.findMany({
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
    return await result;
  }

  async findAll2() {
    return await this.dbService.user.findMany({
      select: {
        name: true,
        posts: {
          select: {
            title: true,
          },
        },
      },
    });
  }

  async findAll3() {
    const result = await this.dbService.user.findMany({
      where: {
        email: {
          contains: 'gmail.com',
        },
        posts: {
          some: {
            published: true,
          },
        },
      },
      include: {
        posts: {
          select: {
            title: true,
            published: true,
          },
        },
      },
    });

    return result;
  }

  async axiosGet1() {
    const response = await axios({
      method: 'GET',
      url: 'https://reqres.in/api/users',
    }).catch(() => {
      throw new ForbiddenException('API not available');
    });
    return { data: response.data?.data };
  }

  async axiosPost1(data: { name: string; job: string }) {
    const response = await axios({
      method: 'POST',
      url: 'https://reqres.in/api/users',
      data: {
        name: data.name,
        job: data.job,
      },
    });

    return {
      data: response.data,
    };
  }
}
